package ca.alexbalt.gameup;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

public class homepage extends AppCompatActivity {

    private BottomNavigationView mMainNav;
    private FrameLayout mMainFrame;

    //initializing fragments
    private HomeFragment homeFragment;
    private NotificationsFragment notificationsFragment;
    private EventFragment eventFragment;
    private MessageFragment messageFragment;
    private AccountFragment accountFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);

        mMainFrame = (FrameLayout) findViewById(R.id.main_frame);
        mMainNav = (BottomNavigationView) findViewById(R.id.main_nav);

        homeFragment = new HomeFragment();
        notificationsFragment = new NotificationsFragment();
        eventFragment = new EventFragment();
        messageFragment = new MessageFragment();
        accountFragment = new AccountFragment();

        //sets home button as default
        setFragment(homeFragment);

        mMainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch(item.getItemId()){
                    //switches the color of the navigation bar whenever that icon is clicked
                    //!!!!!!NEED TO ADD REST OF NAV ICONS
                    case R.id.nav_home:
                        mMainNav.setItemBackgroundResource(R.color.colorPrimaryDark);
                        setFragment(homeFragment);
                        return true;

                    case R.id.nav_notif:
                        mMainNav.setItemBackgroundResource(R.color.colorAccent);
                        setFragment(notificationsFragment);
                        return true;

                    case R.id.nav_events:
                        mMainNav.setItemBackgroundResource(R.color.colorPrimaryDark);
                        setFragment(eventFragment);
                        return true;

                    case R.id.nav_messages:
                        mMainNav.setItemBackgroundResource(R.color.colorAccent);
                        setFragment(messageFragment);
                        return true;

                    case R.id.nav_account:
                        mMainNav.setItemBackgroundResource(R.color.colorPrimaryDark);
                        setFragment(accountFragment);
                        return true;

                    default:
                        return false;

                }
            }
        });

    }

    //methods that change fragments to corresponding,
    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }
}
